# APPLICATIF C# : gestion des données et des réparations
Il s'agit d'un projet scolaire.

### Version 1 à compléter :

Application de gestion des bornes de rechargement (CRUD), Ajout des adhérents.

* V1.1 :

    * ~~Ajouter la notion de latitude/longitude pour les bornes.~~
    * ~~Ajouter la notion de login et mot de passe pour un adhérent lors de sa création~~
    * ~~S’assurer de la présentation de la pièce d’identité de l’adhérent (pas de location possible si la pièce n’est pas présentée) et du paiement de la caution~~

* V1.2 : Finir le CRUD

    * ~~finir la gestion des adhérents (modification et suppression)~~
    * ~~faire le CRUD des vélos (classiques et électriques)~~

### Version 2 à créer :

##### Mode Service :

Lors de la récupération des vélos endommagés par le service technique (leur état est passé en R “en Réparation” via l’application Web), la mairie souhaite connaître exactement les travaux effectués sur chaque vélo via l’application C#.

* V2.1 : 

    * prévoir une connexion spécifique pour l’accès à un nouveau menu de gestion des réparations (prévoir une table UTILISATEURS avec les logins et mots de passe des personnes du service technique)

* V2.2 : 

* Créer une interface de gestion des travaux en précisant le temps de réparation pour chaque travaux, accessible via la connexion précédemment créée. Un même véhicule peut avoir plusieurs réparations en même temps (ex : changer la roue, détordre le guidon)   
Pour cela, modifier la BD pour enregistrer tous les travaux effectués par véhicule :   

    * TRAVAUX ( IdT, LibelleT)
    * RÉPARER (NumV#, IdT#, dateR, tempsR, IdU#) // où IdU est l’identifiant de l’utilisateur connecté

### Version 3 à créer :

##### Mode Service :

* V3 : 

    * Éditer un récapitulatif des réparations avec le temps total passé pour un véhicule donné en format pdf.