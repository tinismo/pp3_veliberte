﻿using System.Windows.Forms;

namespace PPE3_VELIBERTE
{
    partial class FormCRUDVelo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbLatVelo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbLonVelo = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.cbNumV = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(52, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Latitude Vélo :";
            // 
            // tbLatVelo
            // 
            this.tbLatVelo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLatVelo.Location = new System.Drawing.Point(162, 75);
            this.tbLatVelo.Name = "tbLatVelo";
            this.tbLatVelo.Size = new System.Drawing.Size(190, 22);
            this.tbLatVelo.TabIndex = 2;
            this.tbLatVelo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbLatVelo_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(52, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Longitude Vélo :";
            // 
            // tbLonVelo
            // 
            this.tbLonVelo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLonVelo.Location = new System.Drawing.Point(162, 103);
            this.tbLonVelo.Name = "tbLonVelo";
            this.tbLonVelo.Size = new System.Drawing.Size(190, 22);
            this.tbLonVelo.TabIndex = 4;
            this.tbLonVelo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbLonVelo_KeyPress);
            // 
            // btnOK
            // 
            this.btnOK.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnOK.Location = new System.Drawing.Point(162, 162);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 35);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK\r\n";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // cbNumV
            // 
            this.cbNumV.FormattingEnabled = true;
            this.cbNumV.Location = new System.Drawing.Point(162, 48);
            this.cbNumV.Name = "cbNumV";
            this.cbNumV.Size = new System.Drawing.Size(190, 21);
            this.cbNumV.TabIndex = 8;
            this.cbNumV.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(52, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "Numéro Vélo :";
            this.label3.Visible = false;
            // 
            // FormCRUDVelo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(416, 218);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbNumV);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.tbLonVelo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbLatVelo);
            this.Controls.Add(this.label1);
            this.Name = "FormCRUDVelo";
            this.Text = "Ajouter / Modifier : table VELO";
            this.Load += new System.EventHandler(this.FormCRUDVelo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbLatVelo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbLonVelo;
        private System.Windows.Forms.Button btnOK;
        private ComboBox cbNumV;
        private Label label3;

        public TextBox TbLatVelo
        {
            get
            {
                return tbLatVelo;
            }
            set
            {
                tbLatVelo = value;
            }
        }
        public TextBox TbLonVelo
        {
            get
            {
                return tbLonVelo;
            }
            set
            {
                tbLonVelo = value;
            }
        }

        public ComboBox CbNumV
        {
            get
            {
                return cbNumV;
            }

            set
            {
                cbNumV = value;
            }
        }

        public Label Label3
        {
            get
            {
                return label3;
            }

            set
            {
                label3 = value;
            }
        }
    }
}