﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace PPE3_VELIBERTE
{
    public partial class PDFGenerator : Form
    {
        public PDFGenerator()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "PDF file|*.pdf", ValidateNames = true })
            {
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.A4);
                    try
                    {
                        PdfWriter.GetInstance(doc, new FileStream(sfd.FileName, FileMode.Create));
                        doc.Open();
                        doc.Add(new iTextSharp.text.Paragraph(richTextBox1.Text));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                    finally
                    {
                        doc.Close();
                    }
                }
            }
        }

        private void PDFGenerator_Load(object sender, EventArgs e)
        {
            richTextBox1.Text += "RECAPITULATIF DES REPARATIONS : \n\n";
            foreach(int N in Controleur.Vmodele.liste_numV_reparer())
            {
                richTextBox1.Text += "Véhicule " + N + " :\n";
                richTextBox1.Text += " - Réparation effectuée : ";
                foreach (string R in Controleur.Vmodele.liste_reparation(N))
                {
                    richTextBox1.Text += R + ", ";
                }

                richTextBox1.Text += "\n - Temps total passée : " + Controleur.Vmodele.temps_total_reparation(N) + " minutes\n";

                richTextBox1.Text += "\n";
            }
        }
    }
}
