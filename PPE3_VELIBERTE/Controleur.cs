﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;

namespace PPE3_VELIBERTE
{
    /// <summary>
    /// Controleur du projet VELIIBERTE
    /// </summary>
    public static class Controleur
    {
        #region propriétés
        private static Modele vmodele;
        #endregion

        #region accesseurs
        /// <summary>
        /// propriété Vmodele
        /// </summary>
        public static Modele Vmodele
        {
            get { return vmodele; }
            set { vmodele = value; }
        }
        #endregion

        #region methodes
        /// <summary>
        /// instanciation du modele
        /// </summary>
        public static void init()
        {
            Vmodele = new Modele();
        }

        /// <summary>
        /// permet le crud sur la table borne
        /// </summary>
        /// <param name="c">définit l'action : c:create, u update, d delete</param>
        /// <param name="indice">indice de l'élément sélectionné à modifier ou supprimer, -1 si ajout</param>
        public static void crud_borne(Char c, int indice)
        {
            if (c == 'd') // cas de la suppression
            {
                //   DialogResult rep = MessageBox.Show("Etes-vous sûr de vouloir supprimer ce constructeur "+ vmodele.DTConstructeur.Rows[indice][1].ToString()+ " ? ", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                DialogResult rep = MessageBox.Show("Etes-vous sûr de vouloir supprimer cette borne " + vmodele.DT[1].Rows[indice][1].ToString() + " ? ", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (rep == DialogResult.Yes)
                {
                    // on supprime l’élément du DataTable
                    vmodele.DT[1].Rows[indice].Delete();		// suppression dans le DataTable
                    vmodele.DA[1].Update(vmodele.DT[1]);			// mise à jour du DataAdapter
                }
            }
            else
            {
                // cas de l'ajout et modification
                FormCRUDBorne formCRUD = new FormCRUDBorne();  // création de la nouvelle forme
                if (c == 'c')  // mode ajout donc pas de valeur à passer à la nouvelle forme
                {
                    formCRUD.TbNomBorne.Clear();
                    formCRUD.TbNomAdresseRue.Clear();
                    formCRUD.TbnumAdresseRue.Clear();
                    formCRUD.TbLatitudeBorne.Clear();
                    formCRUD.TbLongitudeBorne.Clear();
                }

                if (c == 'u')   // mode update donc on récupère les champs
                {
                    // on remplit les zones par les valeurs du dataGridView correspondantes
                    formCRUD.TbNomBorne.Text = vmodele.DT[1].Rows[indice][1].ToString();
                    formCRUD.TbnumAdresseRue.Text = vmodele.DT[1].Rows[indice][2].ToString();
                    formCRUD.TbNomAdresseRue.Text = vmodele.DT[1].Rows[indice][3].ToString();
                    formCRUD.TbLatitudeBorne.Text = vmodele.DT[1].Rows[indice][4].ToString();
                    formCRUD.TbLongitudeBorne.Text = vmodele.DT[1].Rows[indice][5].ToString();
                }
                // on affiche la nouvelle form
                formCRUD.ShowDialog();

                // si l’utilisateur clique sur OK
                if (formCRUD.DialogResult == DialogResult.OK)
                {
                    if (c == 'c') // ajout
                    {
                        // on crée une nouvelle ligne dans le dataView
                        if (formCRUD.TbNomBorne.Text != "" && formCRUD.TbNomAdresseRue.Text != "")
                        {
                            DataRow NouvLigne = vmodele.DT[1].NewRow();
                            NouvLigne["nomB"] = formCRUD.TbNomBorne.Text;
                            if (formCRUD.TbnumAdresseRue.Text != "") NouvLigne["numRueB"] = formCRUD.TbnumAdresseRue.Text;
                            NouvLigne["nomRueB"] = formCRUD.TbNomAdresseRue.Text;
                            NouvLigne["latitudeB"] = formCRUD.TbLatitudeBorne.Text;
                            NouvLigne["longitudeB"] = formCRUD.TbLongitudeBorne.Text;
                            vmodele.DT[1].Rows.Add(NouvLigne);
                            vmodele.DA[1].Update(vmodele.DT[1]);
                        }
                        else
                            MessageBox.Show("Erreur : il faut saisir au moins la nom et la rue", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    if (c == 'u')  // modif
                    {
                        if (formCRUD.TbNomBorne.Text != "" && formCRUD.TbNomAdresseRue.Text != "")
                        {
                            // on met à jour le dataTable avec les nouvelles valeurs
                            vmodele.DT[1].Rows[indice]["nomB"] = formCRUD.TbNomBorne.Text;
                            if (formCRUD.TbnumAdresseRue.Text != "") vmodele.DT[1].Rows[indice]["numRueB"] = formCRUD.TbnumAdresseRue.Text;
                            vmodele.DT[1].Rows[indice]["nomRueB"] = formCRUD.TbNomAdresseRue.Text;
                            vmodele.DT[1].Rows[indice]["latitudeB"] = formCRUD.TbLatitudeBorne.Text;
                            vmodele.DT[1].Rows[indice]["longitudeB"] = formCRUD.TbLongitudeBorne.Text;
                            vmodele.DA[1].Update(vmodele.DT[1]);
                        }
                        else
                            MessageBox.Show("Erreur : il faut saisir au moins la nom et la rue", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    MessageBox.Show("OK : données enregistrées Borne");
                    formCRUD.Dispose();  // on ferme la form
                }
                else
                {
                    MessageBox.Show("Annulation : aucune donnée enregistrée");
                    formCRUD.Dispose();
                }
            }
        }

        /// <summary>
        /// permet le crud sur la table adherent
        /// </summary>
        /// <param name="c">définit l'action : c:create, u update, d delete </param>
        /// <param name="indice">indice de l'élément sélectionné à modifier ou supprimer, -1 si ajout</param>
        public static void crud_adherent(Char c, int indice)
        {
            if (c == 'd')  // suppression
            {
                //   DialogResult rep = MessageBox.Show("Etes-vous sûr de vouloir supprimer ce constructeur "+ vmodele.DTConstructeur.Rows[indice][1].ToString()+ " ? ", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                DialogResult rep = MessageBox.Show("Etes-vous sûr de vouloir supprimer cet adhérent " + vmodele.DT[2].Rows[indice][1].ToString() + " ? ", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (rep == DialogResult.Yes)
                {
                    // on supprime l’élément du DataTable
                    vmodele.DT[2].Rows[indice].Delete();		// suppression dans le DataTable
                    vmodele.DA[2].Update(vmodele.DT[2]);			// mise à jour du DataAdapter
                }
            }
            else
            {
                FormCRUDAdherent formCRUD = new FormCRUDAdherent();  // création de la nouvelle forme
                if (c == 'c')  // mode ajout donc pas de valeur à passer à la nouvelle forme
                {
                    // à écrire : mettre les contrôles de formCRUD à vide
                    formCRUD.TbLogin.Clear();
                    formCRUD.TbMdp.Clear();
                    formCRUD.TbNom.Clear();
                    formCRUD.TbPrenom.Clear();
                    formCRUD.MtbCP.Clear();
                    formCRUD.TbAdresse.Clear();
                    formCRUD.TbVille.Clear();
                    formCRUD.MtbTel.Clear();
                    formCRUD.MtbTel.Text = "0";
                }

                if (c == 'u')   // mode update donc on récupère les champs
                {
                    // on remplit les zones par les valeurs du dataGridView correspondantes
                    formCRUD.TbNom.Text = vmodele.DT[2].Rows[indice][1].ToString();
                    formCRUD.TbPrenom.Text = vmodele.DT[2].Rows[indice][2].ToString();
                    formCRUD.TbAdresse.Text = vmodele.DT[2].Rows[indice][3].ToString();
                    formCRUD.MtbCP.Text = vmodele.DT[2].Rows[indice][4].ToString();
                    formCRUD.TbVille.Text = vmodele.DT[2].Rows[indice][5].ToString();
                    formCRUD.MtbTel.Text = vmodele.DT[2].Rows[indice][6].ToString();
                    formCRUD.TbLogin.Text = vmodele.DT[2].Rows[indice][7].ToString();
                    formCRUD.TbMdp.Text = vmodele.DT[2].Rows[indice][8].ToString();

                    if (vmodele.DT[2].Rows[indice][9].ToString() == "True")
                    {
                        formCRUD.RbCarteIdentiteOui.Checked = true;
                    }
                    else
                    {
                        formCRUD.RbCarteIdentiteNon.Checked = true;
                    }

                    if (vmodele.DT[2].Rows[indice][10].ToString() == "True")
                    {
                        formCRUD.RbPaiementCautionOui.Checked = true;
                    }
                    else
                    {
                        formCRUD.RbPaiementCautionNon.Checked = true;
                    }
                }

            eti:
                // on affiche la nouvelle form
                formCRUD.ShowDialog();

                // si l’utilisateur clique sur OK
                if (formCRUD.DialogResult == DialogResult.OK)
                {
                    if (c == 'c') // ajout
                    {
                        bool valid = true;
                        // on crée une nouvelle ligne dans le dataView
                        if (formCRUD.TbLogin.Text != "" && formCRUD.TbMdp.Text != "" && formCRUD.TbNom.Text != "" && formCRUD.TbPrenom.Text != "" && formCRUD.MtbCP.Text != "" && formCRUD.TbAdresse.Text != "" && formCRUD.TbVille.Text != "" && formCRUD.MtbTel.Text != "0 /  /  /  /")
                        {
                            DataRow NouvLigne = vmodele.DT[2].NewRow();
                            NouvLigne["loginA"] = formCRUD.TbLogin.Text;
                            NouvLigne["motDePasseA"] = formCRUD.TbMdp.Text;
                            NouvLigne["nomA"] = formCRUD.TbNom.Text;
                            NouvLigne["prenomA"] = formCRUD.TbPrenom.Text;
                            NouvLigne["adresseRueA"] = formCRUD.TbAdresse.Text;

                            if (formCRUD.MtbCP.Text != "")
                            {
                                if (Convert.ToInt32(formCRUD.MtbCP.Text) >= 1000 && Convert.ToInt32(formCRUD.MtbCP.Text) <= 99999)
                                {
                                    NouvLigne["cpA"] = formCRUD.MtbCP.Text;
                                }
                                else valid = false;

                            }

                            NouvLigne["villeA"] = formCRUD.TbVille.Text;

                            if (formCRUD.MtbTel.Text != "0 /  /  /  /")
                            {
                                if (formCRUD.MtbTel.Text.Length == 14) NouvLigne["telA"] = formCRUD.MtbTel.Text;
                                else valid = false;
                            }

                            if (formCRUD.RbCarteIdentiteOui.Checked)
                            {
                                NouvLigne["carteIdentite"] = true;
                            }
                            else
                            {
                                NouvLigne["carteIdentite"] = false;
                            }

                            if (formCRUD.RbPaiementCautionOui.Checked)
                            {
                                NouvLigne["paiementCaution"] = true;
                            }
                            else
                            {
                                NouvLigne["paiementCaution"] = false;
                            }

                            if (valid)
                            {
                                vmodele.DT[2].Rows.Add(NouvLigne);
                                vmodele.DA[2].Update(vmodele.DT[2]);
                            }
                            else
                            {
                                MessageBox.Show("Erreur dans la saisie", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                // ne pas fermer la form : revenir avant le bouton OK
                                goto eti;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Erreur : il faut tout saisir", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            // ne pas fermer la form : revenir avant le bouton OK
                            goto eti;
                        }
                    }

                    if (c == 'u')  // modif
                    {
                        bool valid = true;
                        if (formCRUD.TbLogin.Text != "" && formCRUD.TbMdp.Text != "" && formCRUD.TbNom.Text != "" && formCRUD.TbPrenom.Text != "" && formCRUD.TbAdresse.Text != "" && formCRUD.MtbCP.Text != "" && formCRUD.TbVille.Text != "" && formCRUD.MtbTel.Text != "0 /  /  /  /")
                        {
                            // on met à jour le dataTable avec les nouvelles valeurs
                            vmodele.DT[2].Rows[indice]["loginA"] = formCRUD.TbLogin.Text;
                            vmodele.DT[2].Rows[indice]["motDePasseA"] = formCRUD.TbMdp.Text;
                            vmodele.DT[2].Rows[indice]["nomA"] = formCRUD.TbNom.Text;
                            vmodele.DT[2].Rows[indice]["prenomA"] = formCRUD.TbPrenom.Text;
                            vmodele.DT[2].Rows[indice]["adresseRueA"] = formCRUD.TbAdresse.Text;
                            if (formCRUD.MtbCP.Text != "")
                            {
                                if (Convert.ToInt32(formCRUD.MtbCP.Text) >= 1000 && Convert.ToInt32(formCRUD.MtbCP.Text) <= 99999)
                                {
                                    vmodele.DT[2].Rows[indice]["cpA"] = formCRUD.MtbCP.Text;
                                }
                                else valid = false;

                            }

                            vmodele.DT[2].Rows[indice]["villeA"] = formCRUD.TbVille.Text;

                            if (formCRUD.MtbTel.Text != "0 /  /  /  /")
                            {
                                if (formCRUD.MtbTel.Text.Length == 14) vmodele.DT[2].Rows[indice]["telA"] = formCRUD.MtbTel.Text;
                                else valid = false;
                            }

                            if (formCRUD.RbCarteIdentiteOui.Checked)
                            {
                                vmodele.DT[2].Rows[indice]["carteIdentite"] = true;
                            }
                            else
                            {
                                vmodele.DT[2].Rows[indice]["carteIdentite"] = false;
                            }

                            if (formCRUD.RbPaiementCautionOui.Checked)
                            {
                                vmodele.DT[2].Rows[indice]["paiementCaution"] = true;
                            }
                            else
                            {
                                vmodele.DT[2].Rows[indice]["paiementCaution"] = false;
                            }

                            if (valid)
                            {
                                vmodele.DA[2].Update(vmodele.DT[2]);
                            }
                            else
                            {
                                MessageBox.Show("Erreur dans la saisie", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                // ne pas fermer la form : revenir avant le bouton OK
                                goto eti;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Erreur : il faut tout saisir", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            // ne pas fermer la form : revenir avant le bouton OK
                            goto eti;
                        }
                    }

                    MessageBox.Show("OK : données enregistrées Adherent");
                    formCRUD.Dispose();  // on ferme la form
                }
                else
                {
                    MessageBox.Show("Annulation : aucune donnée enregistrée");
                    formCRUD.Dispose();
                }
            }
        }

        /// <summary>
        /// permet le crud sur la table vehicule
        /// </summary>
        /// <param name="c">définit l'action : c:create, u update, d delete </param>
        /// <param name="indice">indice de l'élément sélectionné à modifier ou supprimer, -1 si ajout</param>
        public static void crud_vehicule(Char c, int indice)
        {
            if (c == 'd') // cas de la suppression
            {
                //   DialogResult rep = MessageBox.Show("Etes-vous sûr de vouloir supprimer ce constructeur "+ vmodele.DTConstructeur.Rows[indice][1].ToString()+ " ? ", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                DialogResult rep = MessageBox.Show("Etes-vous sûr de vouloir supprimer cette borne " + vmodele.DT[3].Rows[indice][1].ToString() + " ? ", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (rep == DialogResult.Yes)
                {
                    // on supprime l’élément du DataTable
                    vmodele.DT[3].Rows[indice].Delete();		// suppression dans le DataTable
                    vmodele.DA[3].Update(vmodele.DT[3]);			// mise à jour du DataAdapter
                }
            }
            else
            {
                // cas de l'ajout et modification
                FormCRUDVehicule formCRUD = new FormCRUDVehicule();  // création de la nouvelle forme
                if (c == 'c')  // mode ajout donc pas de valeur à passer à la nouvelle forme
                {
                    //formCRUD.CbEtatV.Items.Clear();
                }

                if (c == 'u')   // mode update donc on récupère les champs
                {
                    // on remplit les zones par les valeurs du dataGridView correspondantes
                    formCRUD.CbEtatV.Text = vmodele.DT[3].Rows[indice][1].ToString();
                }
                // on affiche la nouvelle form
                formCRUD.ShowDialog();

                // si l’utilisateur clique sur OK
                if (formCRUD.DialogResult == DialogResult.OK)
                {
                    if (c == 'c') // ajout
                    {
                        // on crée une nouvelle ligne dans le dataView
                        if (formCRUD.CbEtatV.Text != "")
                        {
                            DataRow NouvLigne = vmodele.DT[3].NewRow();
                            NouvLigne["etatV"] = formCRUD.CbEtatV.Text;
                            vmodele.DT[3].Rows.Add(NouvLigne);
                            vmodele.DA[3].Update(vmodele.DT[3]);
                        }
                        else
                            MessageBox.Show("Erreur : il faut tout saisir", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    if (c == 'u')  // modif
                    {
                        if (formCRUD.CbEtatV.Text != "")
                        {
                            // on met à jour le dataTable avec les nouvelles valeurs
                            vmodele.DT[3].Rows[indice]["etatV"] = formCRUD.CbEtatV.Text;
                            vmodele.DA[3].Update(vmodele.DT[3]);
                        }
                        else
                            MessageBox.Show("Erreur : il faut tout saisir", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    MessageBox.Show("OK : données enregistrées Véhicule");
                    formCRUD.Dispose();  // on ferme la form
                }
                else
                {
                    MessageBox.Show("Annulation : aucune donnée enregistrée");
                    formCRUD.Dispose();
                }
            }
        }

        /// <summary>
        /// permet le crud sur la table velo
        /// </summary>
        /// <param name="c">définit l'action : c:create, u update, d delete </param>
        /// <param name="indice">indice de l'élément sélectionné à modifier ou supprimer, -1 si ajout</param>
        public static void crud_velo(Char c, int indice)
        {
            if (c == 'd') // cas de la suppression
            {
                //   DialogResult rep = MessageBox.Show("Etes-vous sûr de vouloir supprimer ce constructeur "+ vmodele.DTConstructeur.Rows[indice][1].ToString()+ " ? ", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                DialogResult rep = MessageBox.Show("Etes-vous sûr de vouloir supprimer cette borne " + vmodele.DT[4].Rows[indice][1].ToString() + " ? ", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (rep == DialogResult.Yes)
                {
                    // on supprime l’élément du DataTable
                    vmodele.DT[4].Rows[indice].Delete();		// suppression dans le DataTable
                    vmodele.DA[4].Update(vmodele.DT[4]);			// mise à jour du DataAdapter
                }
            }
            else
            {
                // cas de l'ajout et modification
                FormCRUDVelo formCRUD = new FormCRUDVelo();  // création de la nouvelle forme
                if (c == 'c')  // mode ajout donc pas de valeur à passer à la nouvelle forme
                {
                    formCRUD.Label3.Visible = true;
                    formCRUD.CbNumV.Visible = true;
                    foreach (string unV in Controleur.vmodele.liste_vehicule())
                    {
                        formCRUD.CbNumV.Items.Add(unV);
                    }
                    formCRUD.TbLatVelo.Clear();
                    formCRUD.TbLonVelo.Clear();
                }

                if (c == 'u')   // mode update donc on récupère les champs
                {
                    // on remplit les zones par les valeurs du dataGridView correspondantes
                    formCRUD.TbLatVelo.Text = vmodele.DT[4].Rows[indice][1].ToString();
                    formCRUD.TbLonVelo.Text = vmodele.DT[4].Rows[indice][2].ToString();
                }
                // on affiche la nouvelle form
                formCRUD.ShowDialog();

                // si l’utilisateur clique sur OK
                if (formCRUD.DialogResult == DialogResult.OK)
                {
                    if (c == 'c') // ajout
                    {
                        // on crée une nouvelle ligne dans le dataView
                        if (formCRUD.CbNumV.Text != "" && formCRUD.TbLatVelo.Text != "" && formCRUD.TbLonVelo.Text != "")
                        {
                            DataRow NouvLigne = vmodele.DT[4].NewRow();
                            NouvLigne["numV"] = formCRUD.CbNumV.Text;
                            NouvLigne["latitudeV"] = formCRUD.TbLatVelo.Text;
                            NouvLigne["longitudeV"] = formCRUD.TbLonVelo.Text;
                            vmodele.DT[4].Rows.Add(NouvLigne);
                            vmodele.DA[4].Update(vmodele.DT[4]);
                        }
                        else
                            MessageBox.Show("Erreur : il faut tout saisir", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    if (c == 'u')  // modif
                    {
                        if (formCRUD.TbLatVelo.Text != "" && formCRUD.TbLonVelo.Text != "")
                        {
                            // on met à jour le dataTable avec les nouvelles valeurs
                            vmodele.DT[4].Rows[indice]["latitudeV"] = formCRUD.TbLatVelo.Text;
                            vmodele.DT[4].Rows[indice]["longitudeV"] = formCRUD.TbLonVelo.Text;
                            vmodele.DA[4].Update(vmodele.DT[4]);
                        }
                        else
                            MessageBox.Show("Erreur : il faut tout saisir", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    MessageBox.Show("OK : données enregistrées Velo");
                    formCRUD.Dispose();  // on ferme la form
                }
                else
                {
                    MessageBox.Show("Annulation : aucune donnée enregistrée");
                    formCRUD.Dispose();
                }
            }
        }

        /// <summary>
        /// permet le crud sur la table velo électrique
        /// </summary>
        /// <param name="c">définit l'action : c:create, u update, d delete </param>
        /// <param name="indice">indice de l'élément sélectionné à modifier ou supprimer, -1 si ajout</param>
        public static void crud_veloelectrique(Char c, int indice)
        {
            if (c == 'd') // cas de la suppression
            {
                //   DialogResult rep = MessageBox.Show("Etes-vous sûr de vouloir supprimer ce constructeur "+ vmodele.DTConstructeur.Rows[indice][1].ToString()+ " ? ", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                DialogResult rep = MessageBox.Show("Etes-vous sûr de vouloir supprimer cette borne " + vmodele.DT[5].Rows[indice][1].ToString() + " ? ", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (rep == DialogResult.Yes)
                {
                    // on supprime l’élément du DataTable
                    vmodele.DT[5].Rows[indice].Delete();		// suppression dans le DataTable
                    vmodele.DA[5].Update(vmodele.DT[5]);			// mise à jour du DataAdapter
                }
            }
            else
            {
                // cas de l'ajout et modification
                FormCRUDVeloElectrique formCRUD = new FormCRUDVeloElectrique();  // création de la nouvelle forme
                if (c == 'c')  // mode ajout donc pas de valeur à passer à la nouvelle forme
                {
                    formCRUD.Label3.Visible = true;
                    formCRUD.CbNumV.Visible = true;
                    foreach (string unV in Controleur.vmodele.liste_vehicule())
                    {
                        formCRUD.CbNumV.Items.Add(unV);
                    }

                    foreach (string unB in Controleur.vmodele.liste_borne())
                    {
                        formCRUD.CbNumB.Items.Add(unB);
                    }
                }

                if (c == 'u')   // mode update donc on récupère les champs
                {
                    // on remplit les zones par les valeurs du dataGridView correspondantes
                    //formCRUD.CbNumB.Text = vmodele.DT[5].Rows[indice][1].ToString();
                    foreach (string unB in Controleur.vmodele.liste_borne())
                    {
                        formCRUD.CbNumB.Items.Add(unB);
                    }

                    formCRUD.CbNumB.Text = Controleur.vmodele.nom_borne(Convert.ToInt32(vmodele.DT[5].Rows[indice][1]));
                }
                // on affiche la nouvelle form
                formCRUD.ShowDialog();

                // si l’utilisateur clique sur OK
                if (formCRUD.DialogResult == DialogResult.OK)
                {
                    if (c == 'c') // ajout
                    {
                        // on crée une nouvelle ligne dans le dataView
                        if (formCRUD.CbNumB.Text != "" && formCRUD.CbNumB.Text != "")
                        {
                            DataRow NouvLigne = vmodele.DT[5].NewRow();
                            NouvLigne["numV"] = formCRUD.CbNumV.Text;
                            NouvLigne["numB"] = Controleur.vmodele.id_borne(formCRUD.CbNumB.Text);
                            vmodele.DT[5].Rows.Add(NouvLigne);
                            vmodele.DA[5].Update(vmodele.DT[5]);
                        }
                        else
                            MessageBox.Show("Erreur : il faut tout saisir", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    if (c == 'u')  // modif
                    {
                        if (formCRUD.CbNumB.Text != "")
                        {
                            // on met à jour le dataTable avec les nouvelles valeurs
                            vmodele.DT[5].Rows[indice]["numB"] = Controleur.vmodele.id_borne(formCRUD.CbNumB.Text);
                            vmodele.DA[5].Update(vmodele.DT[5]);
                        }
                        else
                            MessageBox.Show("Erreur : il faut tout saisir", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    MessageBox.Show("OK : données enregistrées Velo");
                    formCRUD.Dispose();  // on ferme la form
                }
                else
                {
                    MessageBox.Show("Annulation : aucune donnée enregistrée");
                    formCRUD.Dispose();
                }
            }
        }
        #endregion
    }
}
